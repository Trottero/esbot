package Skills;
import Core.*;

import javax.rmi.CORBA.Util;
import java.awt.*;

/**
 * Created by wrlxs on 30-5-2017.
 */
public class Summoning {

    public static void main(String[] args) throws AWTException {
        new Summoning(false).start();
    }
    //AFK at bandits or monkyeguards with summoning pet out + prayer on melee and make sure that the inventory tab is selected!
    //dont forget your potions.
    private ActorBot bot;
    private Coord[] checkcoords;
    private boolean trail = false;
    //initialization of the bot
    public Summoning (boolean trail){
        this.trail = trail;
        checkcoords = new Coord[15];
        //sets the coords to be checked to a line
        for (int i = 0; i < 15; i++) {
            checkcoords[i] = new Coord(1715 + i, 115);
        }

        //creating bot
        try {
            bot = new ActorBot();
        } catch (AWTException e) {
            e.printStackTrace();
        }

    }

    public void start (){
        int count = 0;
        while(true){
            Utils.sleepSecond(5);

            if(needsDrinking()){
                drinkPot();
                Utils.sleepSecond(1);
                bot.moveMouseToMiddle();
                count++;
            }
//            if(trail = true && count > 2){
//                bot.portHome();
//                System.exit(0);
//            }
        }
    }

    public boolean needsDrinking(){
        //red pot checking
        for (int i = 0; i < checkcoords.length; i++) {
            Color color = bot.getPixelColor(checkcoords[i].getX(), checkcoords[i].getY());
            if (color.getRed() == 245 && color.getGreen() == 13 && color.getBlue() == 13) {
                System.out.println("Red text spotted! Attempting to drink prayer pot!");
                return true;
            }
        }
        return false;
    }

    public void drinkPot() {
        Coord potCoord = bot.findItem(Item.SUPER_RESTORE, 1);
        if(potCoord != null){
            bot.click(potCoord.getX(), potCoord.getY());
            Utils.sleepMS(200);
            bot.moveMouseToMiddle();
            return;
        }
        potCoord = bot.findItem(Item.PRAYER_POTION, 1);
        if(potCoord != null){
            bot.click(potCoord.getX(), potCoord.getY());
            Utils.sleepMS(200);
            bot.moveMouseToMiddle();
            return;
        }

        bot.portHome();
        System.exit(0);

    }
}
