package Skills;

import Core.ActorBot;
import Core.Coord;
import Core.Item;
import Core.Utils;

import java.awt.*;
import java.awt.event.InputEvent;
import java.awt.event.KeyEvent;
import java.util.HashMap;

/**
 * Created by wrlxs on 12-6-2017.
 */
public class SmithingBars {
    public static void main(String[] args) throws AWTException {
        new SmithingBars();
    }

    Item[] raw_items = {Item.ORE_MITHRIL, Item.ORE_ADAMANTITE, Item.ORE_RUNITE};
    Item[] cooked_items = {Item.BAR_MITHRIL, Item.BAR_ADAMANTITE, Item.BAR_RUNITE};
    Coord[] dialogCoords = {new Coord(345, 933), new Coord(404, 933), new Coord(460, 933)};
    Item raw_item = Item.RAW_SHARK;
    HashMap hm = new HashMap();

    ActorBot bot;
    public SmithingBars(){

        for (int i = 0; i < dialogCoords.length; i++) {
            hm.put(raw_items[i].itemName, dialogCoords[i]);
        }
        try {
            bot = new ActorBot();
        } catch (AWTException e) {
            e.printStackTrace();
        }

        Utils.sleepSecond(5);

        while(true){
            bot.openShop();

            for (int i = 0; i < cooked_items.length; i++) {
                if(bot.findItem(cooked_items[i], 1)  != null){
                    bot.sellXOfItem(cooked_items[i]);
                }
            }

            boolean foundItem = false;
            for (int i = 0; i < raw_items.length; i++) {
                if(bot.findItem(raw_items[i], 2) != null){
                    foundItem = true;
                    bot.buyItemFromShop(raw_items[i]);
                    raw_item = raw_items[i];
                    break;
                }
            }

            bot.closeShop();
            if (!foundItem){

                System.out.println("None of the items could be found; exiting.");
                System.exit(0);
            }
            smithDatShit();
            Utils.sleepSecond(65);
            //smithDatShit();
           // Utils.sleepSecond(50);
        }
    }

    public void smithDatShit(){
        bot.click(900, 530);
        Color color = bot.getPixelColor(400, 900);
        while(!Utils.isBetween(color.getRed(),214, 5) && !Utils.isBetween(color.getGreen(),195, 5) && !Utils.isBetween(color.getBlue(),165, 5)){
            Utils.sleepMS(500);
            color = bot.getPixelColor(400, 900);
        }

        Utils.sleepMS(200);
        bot.mouseMove(((Coord) hm.get(raw_item.itemName)).getX(), 933);
        Utils.sleepMS(100);
        bot.mousePress(InputEvent.BUTTON3_MASK);
        bot.mouseRelease(InputEvent.BUTTON3_MASK);
        Utils.sleepMS(300);
        bot.click(((Coord) hm.get(raw_item.itemName)).getX(), 933 + 73);
        Utils.sleepMS(1000);
        bot.keyPress(KeyEvent.VK_2);
        bot.keyRelease(KeyEvent.VK_2);
        bot.keyPress(KeyEvent.VK_8);
        bot.keyRelease(KeyEvent.VK_8);
        bot.keyPress(KeyEvent.VK_ENTER);
        bot.keyRelease(KeyEvent.VK_ENTER);
        Utils.sleepMS(300);
    }

}
