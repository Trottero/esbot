package Skills;

import Core.ActorBot;
import Core.Coord;
import Core.Item;
import Core.Utils;

import javax.rmi.CORBA.Util;
import java.awt.*;

/**
 * Created by wrlxs on 3-6-2017.
 */
public class Mining55_75 {
    ActorBot bot;
    int location = 0;

    Item[] ore_items = {Item.ORE_MITHRIL, Item.ORE_ADAMANTITE};
    public static void main(String[] args) throws AWTException {
        new Mining55_75();
    }

    Mining55_75(){

        try {
            bot = new ActorBot();
        } catch (AWTException e) {
            e.printStackTrace();
        }
        Utils.sleepSecond(5);

        while(true) {
            bot.openShop();

            for (int i = 0; i < ore_items.length; i++) {
                if (bot.findItem(ore_items[i], 1) != null) {
                    bot.sellXOfItem(ore_items[i]);
                }
            }
            Utils.sleepMS(200);
            bot.closeShop();
            Utils.sleepMS(200);
            mineOre();
        }

    }

    private void mineOre(){
        int timer = 0;
        int wait = 37;
        Utils.sleepMS(2500);
        bot.click(965, 485);
        while(timer < wait){
            Utils.sleepMS(1000);
            if(checkOreEmpty()){
                if(location == 0){
                    bot.click(1014, 527);
                    Utils.sleepMS(2500);
                    location = 1;
                } else if(location == 1){
                    bot.click(917, 538);
                    Utils.sleepMS(2500);
                    location = 0;
                }
                bot.click(965, 485);
                wait++;
                wait++;

            }
            timer++;
            System.out.println(timer);
        }
    }

    public boolean checkOreEmpty(){
        int[] xCoords = {948, 949, 950, 951, 952};
        Color color;
        for (int i = 0; i < xCoords.length; i++) {
            for (int j = 0; j < 20; j++) {
                color  = bot.getPixelColor(xCoords[i], 470 + j);
                if(Utils.isBetween(color.getRed(), 65, 5) && Utils.isBetween(color.getGreen(), 55, 5) && Utils.isBetween(color.getBlue(), 55, 10)){
                    return true;
                }
            }
        }
        return false;
    }
}
