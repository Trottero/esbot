package Skills;

import Core.ActorBot;
import Core.Coord;
import Core.Item;
import Core.Utils;

import java.awt.*;
import java.awt.event.InputEvent;

/**
 * Created by wrlxs on 31-5-2017.
 */
public class DZfishing {
    public static void main(String[] args) throws AWTException {
        new DZfishing();
    }

    ActorBot bot;

    Item[] raw_items = {Item.RAW_ROCKTAIL};
    Item[] cooked_items = {Item.COOKED_ROCKTAIL};
    //Item[] burnt_items = {Item.BURNT_TUNA, Item.BURNT_SWORDFISH, Item.BURNT_SHARK};
    Item raw_item = Item.RAW_SHARK;
    public DZfishing() {
        try {
            bot = new ActorBot();
        } catch (AWTException e) {
            e.printStackTrace();
        }

        //982 319
        //74 67 51
        Utils.sleepSecond(5);


        while (true) {

            bot.openShop();

            for (int i = 0; i < raw_items.length; i++) {
                if(bot.findItem(raw_items[i], 1)  != null){
                    bot.sellXOfItem(raw_items[i]);
                }
            }

            bot.closeShop();

            fishIt();
        }
    }
    public void fishIt(){
        Utils.sleepMS(3000);
        bot.click(907, 550  );

        Utils.sleepSecond(73);

    }

}
