package Skills;

import Core.*;

import java.awt.*;
import java.awt.event.InputEvent;
import java.awt.event.KeyEvent;

/**
 * Created by Niels on 14-6-2017.
 */
public class AutoBossing {
    public static void main(String[] args) throws AWTException {
        new AutoBossing();
    }

    ActorBot bot;
    boolean donebefore = false;
    public AutoBossing(){
        try {
            bot = new ActorBot();
        } catch (AWTException e) {
            e.printStackTrace();
        }

        int currentBossHP;
        Color[] colors2 = {new Color(0,255,0), new Color(255,255,0), new Color(252,166,7), new Color(245,13,13)};
        Color[] colors = {new Color(255,0,0), new Color(255,255,255), new Color(0,255,0)/*, new Color(255,204,51)*/};
        Rectangle screenRect = new Rectangle(15, 70, 370, 18);
        Rectangle healthRect = new Rectangle(1712, 79, 23, 8);
        Rectangle prayRect = new Rectangle(1709, 110, 23, 20);

        ImageRecognizer bossRegion = new ImageRecognizer(screenRect, "font0", colors);
        ImageRecognizer healthRegion = new ImageRecognizer(healthRect, "font1", colors2);
        ImageRecognizer prayRegion = new ImageRecognizer(prayRect, "font1", colors2);

        Utils.sleepSecond(5);
        //this loop will be ran every second. Use this to check for actions.
        while(true){

            String endResult = bossRegion.doOCR();
            String endResult2 = healthRegion.doOCR();
            String endResult3 = prayRegion.doOCR();


            //converts data Exception isnt caught here.
            int currentHP = Integer.parseInt(endResult2);
            int currentPray = Integer.parseInt(endResult3);

            //begin the processsing of the bossRegion
            int[] nums = {0,1,2,3,4,5,6,7,8,9};
            String endendString = "";
            for (int i = 0; i < endResult.length(); i++) {
                if(Character.toString(endResult.charAt(i)).equals("/")){
                    break;
                }
                for (int j = 0; j < nums.length; j++) {
                    if(Character.toString(endResult.charAt(i)).equals(Integer.toString(nums[j]))){
                        endendString = endendString + endResult.charAt(i);
                    }
                }
            }

            //default it just in case.
            if(!endendString.equals("")){
                currentBossHP = Integer.parseInt(endendString);
                System.out.println(currentBossHP +" | " + currentHP + " | " + currentPray);
            } else {
                currentBossHP = -10;
            }



            //everything prepared, do the normal loop shit now
            //attack the fucking boss xd
            if(currentBossHP == 1500){
                Utils.sleepMS(100);
                bot.mouseMove(495, 675);
                bot.mousePress(InputEvent.BUTTON3_MASK);
                bot.mouseRelease(InputEvent.BUTTON3_MASK);
                Utils.sleepMS(200);
                bot.click(495, 675 + 60);
                Utils.sleepMS(1000);
            }
            if(currentBossHP > 1480){
                Coord coord = bot.findItem(Item.SEERS_RING, 1);
                if(coord != null){
                    bot.click(coord.getX(), coord.getY());
                    Utils.sleepMS(200);
                    bot.moveMouseToMiddle();
                }
                donebefore = false;
            } else if (currentBossHP < 100 && currentBossHP > -1){
                Coord coord = bot.findItem(Item.RING_OF_WEALTH, 1);
                if(coord != null){
                    bot.click(coord.getX(), coord.getY());
                    Utils.sleepMS(200);
                    bot.moveMouseToMiddle();
                }
                donebefore = false;

                //After the boss dies, this part of the code will be ran.
            } else if(currentBossHP == -10){
                if(!donebefore){
                    donebefore = true;
                    Utils.sleepMS(5000);
                    bot.keyPress(KeyEvent.VK_F3);
                    bot.keyRelease(KeyEvent.VK_F3);
                    Utils.sleepMS(500);
                    bot.click(1815, 800);
                    Utils.sleepMS(200);
                    bot.click(1815, 832);
                    Utils.sleepMS(200);
                    bot.click(1850, 832);
                    Utils.sleepMS(200);
                    bot.keyPress(KeyEvent.VK_ESCAPE);
                    bot.keyRelease(KeyEvent.VK_ESCAPE);

                    bot.closeDialogIfOpen();
                    bot.portHome();
                    bot.openShop();
                    bot.buyItemFromShop(Item.COOKED_SHARK);
                    bot.closeShop();
                    bot.portBossing();
                    Utils.sleepSecond(5);
                    bot.clickCompass();
                    Utils.sleepMS(500);
                    bot.keyPress(KeyEvent.VK_UP);
                    Utils.sleepSecond(1);
                    bot.keyRelease(KeyEvent.VK_UP);

                    //TODO: Make it move to the correct position.
                    bot.click(1190,377);
                }
            }


            //check if the player is healthy
            if(currentHP < 500){
                Item[] restoringItems = {Item.COOKED_SHARK, Item.COOKED_ROCKTAIL};
                for (int i = 0; i < restoringItems.length; i++) {
                    Coord coord = bot.findItem(restoringItems[i], 1);
                    if(coord != null) {
                        bot.click(coord.getX(), coord.getY());
                        Utils.sleepMS(200);
                        bot.moveMouseToMiddle();
                        break;
                    }
                }
            }
            if(currentPray < 10){
                Item[] restoringItems = {Item.SUPER_RESTORE, Item.PRAYER_POTION};
                for (int i = 0; i < restoringItems.length; i++) {
                    Coord coord = bot.findItem(restoringItems[i], 1);
                    if(coord != null) {
                        bot.click(coord.getX(), coord.getY());
                        Utils.sleepMS(200);
                        bot.moveMouseToMiddle();
                        break;
                    }
                }
            }
            Utils.sleepSecond(1);
        }
    }
}
