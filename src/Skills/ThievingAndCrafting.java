package Skills;

import Core.ActorBot;
import Core.Coord;
import Core.Item;
import Core.Utils;

import java.awt.*;
import java.awt.event.KeyEvent;
import java.util.ArrayList;

/**
 * Created by Niels on 6-7-2017.
 */
public class ThievingAndCrafting {
    /*first up this script will change the orientation of the screen so it wont mess up!
    * The stalls can be qutie a bitch and walk you around them when you are just clicking on them
    * be sure to stand in front of the stall at the thieving teleport.
    * THIS SCRIPT ONLY WORKS FOR THE ONYX STALL.
    * THIS SCRIPT RELIES ON YOU HAVING THE CHISEL IN THE FIRST SLOT SO PLEASE DO THAT.
    */

    ActorBot bot;

    //initialize the script set variables. Will also start the loop after 5 seconds
    public ThievingAndCrafting(){
        try {
            bot = new ActorBot();
        } catch (AWTException e) {
            e.printStackTrace();
        }
        Utils.sleepSecond(5);
        bot.clickCompass();
        Utils.sleepMS(500);
        bot.keyPress(KeyEvent.VK_UP);
        Utils.sleepSecond(1);
        bot.keyRelease(KeyEvent.VK_UP);

        while(true){
            System.out.println("Entering thieving mode!");
            ArrayList<Coord> onyxs = bot.findAllOfItem(Item.CUT_ONYX, 1);
            int count = 0;
            int lasSize = 0;
            while(onyxs.size() < 25 && count < 25){
                if (lasSize == onyxs.size()){
                    count++;
                } else {
                    count = 0;
                    lasSize = onyxs.size();
                }
                bot.click(1010, 538);
                onyxs = bot.findAllOfItem(Item.CUT_ONYX, 1);
                Utils.sleepMS(500);
            }
            if(count >= 25){
                System.err.println("The count is over 25! Shutting downscript!");
                break;
            }
            System.out.println("Leaving thieving mode total amount of onyx's: " + onyxs.size());

            //craft onyxs using chisel.
            for (int i = 0; i < onyxs.size(); i++) {
                bot.click(1753, 764);
                Utils.sleepMS(100);
                bot.click(onyxs.get(i));
                Utils.sleepMS(1600);
            }
        }
    }
    public static void main(String[] args) throws AWTException {
        new ThievingAndCrafting();
    }
}
