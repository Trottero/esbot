package Skills;

import Core.ActorBot;
import Core.Item;
import Core.Utils;
import Core.Coord;

import java.awt.*;
import java.awt.event.InputEvent;

/**
 * Created by wrlxs on 31-5-2017.
 */
public class Cooking {
    public static void main(String[] args) throws AWTException {
        new Cooking();
    }

    ActorBot bot;

    Item[] raw_items = {Item.RAW_TUNA, Item.RAW_SWORDFISH, Item.RAW_SHARK};
    Item[] cooked_items = {Item.COOKED_TUNA, Item.COOKED_SWORDFISH, Item.COOKED_SHARK};
    Item[] burnt_items = {Item.BURNT_TUNA, Item.BURNT_SWORDFISH, Item.BURNT_SHARK};
    Item raw_item = Item.RAW_SHARK;
    public Cooking() {
        try {
            bot = new ActorBot();
        } catch (AWTException e) {
            e.printStackTrace();
        }

        //982 319
        //74 67 51
        Utils.sleepSecond(5);


        while (true) {
            for (int i = 0; i < burnt_items.length; i++) {
                bot.dropAllOf(burnt_items[i]);
            }

            bot.openShop();

            for (int i = 0; i < cooked_items.length; i++) {
                if(bot.findItem(cooked_items[i], 1)  != null){
                    bot.sellXOfItem(cooked_items[i]);
                }
            }

            boolean foundItem = false;
            for (int i = 0; i < raw_items.length; i++) {
                if(bot.findItem(raw_items[i], 2) != null){
                    foundItem = true;
                    bot.buyItemFromShop(raw_items[i]);
                    raw_item = raw_items[i];
                    break;
                }
            }
            bot.closeShop();
            if (!foundItem){
                System.out.println("None of the items could be found; exiting.");
                System.exit(0);
            }

            cookIt();
            Utils.sleepMS(2000);
            cookIt();
            Utils.sleepSecond(38);
        }
    }
    public void cookIt(){
        Coord coord = bot.findItem(raw_item, 1);
        bot.click(coord.getX(), coord.getY());
        Utils.sleepMS(100);
        bot.click(1005, 535);

        //checks for thing to come up
        Color color = bot.getPixelColor(400, 900);
        while(!Utils.isBetween(color.getRed(),214, 5) && !Utils.isBetween(color.getGreen(),195, 5) && !Utils.isBetween(color.getBlue(),165, 5)){
            Utils.sleepMS(500);
            color = bot.getPixelColor(400, 900);
        }
        Utils.sleepMS(200);
        bot.mouseMove(260, 933);
        Utils.sleepMS(100);
        bot.mousePress(InputEvent.BUTTON3_MASK);
        bot.mouseRelease(InputEvent.BUTTON3_MASK);
        Utils.sleepMS(300);
        bot.click(260, 933 + 73);
        Utils.sleepMS(100);

    }
}
