package Skills;

import Core.ActorBot;
import Core.Coord;
import Core.Item;
import Core.Utils;

import java.awt.*;
import java.awt.event.InputEvent;

/**
 * Created by wrlxs on 31-5-2017.
 */
public class DZCooking {
    public static void main(String[] args) throws AWTException {
        new DZCooking();
    }

    ActorBot bot;

    Item[] raw_items = {Item.RAW_ROCKTAIL};
    Item[] cooked_items = {Item.COOKED_ROCKTAIL};
    //Item[] burnt_items = {Item.BURNT_TUNA, Item.BURNT_SWORDFISH, Item.BURNT_SHARK};
    Item raw_item = Item.RAW_SHARK;
    public DZCooking() {
        try {
            bot = new ActorBot();
        } catch (AWTException e) {
            e.printStackTrace();
        }

        //982 319
        //74 67 51
        Utils.sleepSecond(5);


        while (true) {
//            for (int i = 0; i < burnt_items.length; i++) {
//                bot.dropAllOf(burnt_items[i]);
//            }
            banktheshit();

            bot.openShop();

//            for (int i = 0; i < cooked_items.length; i++) {
//                if(bot.findItem(cooked_items[i], 1)  != null){
//                    bot.sellXOfItem(cooked_items[i]);
//                }
//            }

            boolean foundItem = false;
            for (int i = 0; i < raw_items.length; i++) {
                if(bot.findItem(raw_items[i], 2) != null){
                    foundItem = true;
                    bot.buyItemFromShop(raw_items[i]);
                    raw_item = raw_items[i];
                    break;
                }
            }
            bot.closeShop();
            if (!foundItem){
                System.out.println("None of the items could be found; exiting.");
                System.exit(0);
            }

            //TODO: ACTUALLY FUCKING FIX THIS
            cookIt();
            Utils.sleepMS(2100);
            cookIt();
            Utils.sleepSecond(36);
        }
    }
    public void cookIt(){
        Coord coord = bot.findItem(raw_item, 1);
        bot.click(coord.getX(), coord.getY());
        Utils.sleepMS(100);
        bot.click(1020, 535);

        //checks for thing to come up
        Color color = bot.getPixelColor(400, 900);
        while(!Utils.isBetween(color.getRed(),214, 5) && !Utils.isBetween(color.getGreen(),195, 5) && !Utils.isBetween(color.getBlue(),165, 5)){
            Utils.sleepMS(500);
            color = bot.getPixelColor(400, 900);
        }
        Utils.sleepMS(200);
        bot.mouseMove(260, 933);
        Utils.sleepMS(100);
        bot.mousePress(InputEvent.BUTTON3_MASK);
        bot.mouseRelease(InputEvent.BUTTON3_MASK);
        Utils.sleepMS(300);
        bot.click(260, 933 + 73);
        Utils.sleepMS(100);

    }

    public void banktheshit(){
        bot.click(958,309);
        Color color = bot.getPixelColor(982, 319);
        while(!Utils.isBetween(color.getRed(),74, 5) && !Utils.isBetween(color.getGreen(),67, 5) && !Utils.isBetween(color.getBlue(),51, 5)) {
            System.out.println("Waiting for shop to open");
            Utils.sleepMS(500);
            color = bot.getPixelColor(982, 319);
        }
        bot.click(1000, 600);
        Utils.sleepMS(500);
        bot.closeShop();
        bot.click(963,742);
        Utils.sleepMS(2000);
    }
}
