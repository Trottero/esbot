package Core;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by Niels on 19-6-2017.
 */
public class testSet {
    String font = "";
    HashMap<BufferedImage, String> imgToInt = new HashMap<>();
    ArrayList<BufferedImage> traindata = new ArrayList<>();
    public testSet(String font){
        this.font = font;
        for (int i = 0; i < 10; i++) {
            try {
                traindata.add(ImageIO.read(new File("images\\"+font+"\\" +i+".PNG")));
            } catch (IOException e) {
                e.printStackTrace();
            }
            imgToInt.put(traindata.get(i), Integer.toString(i));
        }
        try {
            traindata.add(ImageIO.read(new File("images\\"+font+"\\" +"FORWARD_SLASH"+".PNG")));
            imgToInt.put(traindata.get(traindata.size() - 1), "/");
        } catch (IOException e) {
            e.printStackTrace();
        }
        try {
            traindata.add(ImageIO.read(new File("images\\"+font+"\\" +"PIPELINE"+".PNG")));
            imgToInt.put(traindata.get(traindata.size() - 1), "|");
        } catch (IOException e) {
            e.printStackTrace();
        }
        try {
            traindata.add(ImageIO.read(new File("images\\"+font+"\\" +"cg"+".PNG")));
            imgToInt.put(traindata.get(traindata.size() - 1), "G");
        } catch (IOException e) {
            e.printStackTrace();
        }
        try {
            traindata.add(ImageIO.read(new File("images\\"+font+"\\" +"e"+".PNG")));
            imgToInt.put(traindata.get(traindata.size() - 1), "e");
        } catch (IOException e) {
            e.printStackTrace();
        }
        try {
            traindata.add(ImageIO.read(new File("images\\"+font+"\\" +"n"+".PNG")));
            imgToInt.put(traindata.get(traindata.size() - 1), "n");
        } catch (IOException e) {
            e.printStackTrace();
        }
        try {
            traindata.add(ImageIO.read(new File("images\\"+font+"\\" +"r"+".PNG")));
            imgToInt.put(traindata.get(traindata.size() - 1), "r");
        } catch (IOException e) {
            e.printStackTrace();
        }
        try {
            traindata.add(ImageIO.read(new File("images\\"+font+"\\" +"a"+".PNG")));
            imgToInt.put(traindata.get(traindata.size() - 1), "a");
        } catch (IOException e) {
            e.printStackTrace();
        }
        try {
            traindata.add(ImageIO.read(new File("images\\"+font+"\\" +"l"+".PNG")));
            imgToInt.put(traindata.get(traindata.size() - 1), "l");
        } catch (IOException e) {
            e.printStackTrace();
        }
        try {
            traindata.add(ImageIO.read(new File("images\\"+font+"\\" +"d"+".PNG")));
            imgToInt.put(traindata.get(traindata.size() - 1), "d");
        } catch (IOException e) {
            e.printStackTrace();
        }
        try {
            traindata.add(ImageIO.read(new File("images\\"+font+"\\" +"o"+".PNG")));
            imgToInt.put(traindata.get(traindata.size() - 1), "o");
        } catch (IOException e) {
            e.printStackTrace();
        }
        try {
            traindata.add(ImageIO.read(new File("images\\"+font+"\\" +"ch"+".PNG")));
            imgToInt.put(traindata.get(traindata.size() - 1), "H");
        } catch (IOException e) {
            e.printStackTrace();
        }
        try {
            traindata.add(ImageIO.read(new File("images\\"+font+"\\" +"t"+".PNG")));
            imgToInt.put(traindata.get(traindata.size() - 1), "t");
        } catch (IOException e) {
            e.printStackTrace();
        }
        try {
            traindata.add(ImageIO.read(new File("images\\"+font+"\\" +"h"+".PNG")));
            imgToInt.put(traindata.get(traindata.size() - 1), "h");
        } catch (IOException e) {
            e.printStackTrace();
        }
        try {
            traindata.add(ImageIO.read(new File("images\\"+font+"\\" +"semicolon"+".PNG")));
            imgToInt.put(traindata.get(traindata.size() - 1), ":");
        } catch (IOException e) {
            e.printStackTrace();
        }
        try {
            traindata.add(ImageIO.read(new File("images\\"+font+"\\" +"ck"+".PNG")));
            imgToInt.put(traindata.get(traindata.size() - 1), "K");
        } catch (IOException e) {
            e.printStackTrace();
        }
        try {
            traindata.add(ImageIO.read(new File("images\\"+font+"\\" +"i"+".PNG")));
            imgToInt.put(traindata.get(traindata.size() - 1), "i");
        } catch (IOException e) {
            e.printStackTrace();
        }
        try {
            traindata.add(ImageIO.read(new File("images\\"+font+"\\" +"s"+".PNG")));
            imgToInt.put(traindata.get(traindata.size() - 1), "s");
        } catch (IOException e) {
            e.printStackTrace();
        }
        try {
            traindata.add(ImageIO.read(new File("images\\"+font+"\\" +"cc"+".PNG")));
            imgToInt.put(traindata.get(traindata.size() - 1), "C");
        } catch (IOException e) {
            e.printStackTrace();
        }
        try {
            traindata.add(ImageIO.read(new File("images\\"+font+"\\" +"p"+".PNG")));
            imgToInt.put(traindata.get(traindata.size() - 1), "p");
        } catch (IOException e) {
            e.printStackTrace();
        }
        try {
            traindata.add(ImageIO.read(new File("images\\"+font+"\\" +"cb"+".PNG")));
            imgToInt.put(traindata.get(traindata.size() - 1), "B");
        } catch (IOException e) {
            e.printStackTrace();
        }
        try {
            traindata.add(ImageIO.read(new File("images\\"+font+"\\" +"u"+".PNG")));
            imgToInt.put(traindata.get(traindata.size() - 1), "u");
        } catch (IOException e) {
            e.printStackTrace();
        }
        try {
            traindata.add(ImageIO.read(new File("images\\"+font+"\\" +"cm"+".PNG")));
            imgToInt.put(traindata.get(traindata.size() - 1), "M");
        } catch (IOException e) {
            e.printStackTrace();
        }
        try {
            traindata.add(ImageIO.read(new File("images\\"+font+"\\" +"cn"+".PNG")));
            imgToInt.put(traindata.get(traindata.size() - 1), "n");
        } catch (IOException e) {
            e.printStackTrace();
        }
        try {
            traindata.add(ImageIO.read(new File("images\\"+font+"\\" +"x"+".PNG")));
            imgToInt.put(traindata.get(traindata.size() - 1), "x");
        } catch (IOException e) {
            e.printStackTrace();
        }




    }

    public ArrayList<BufferedImage> getTraindata() {
        return traindata;
    }

    public HashMap<BufferedImage, String> getImgToInt() {
        return imgToInt;
    }

    public String getFont() {
        return font;
    }
}
