package Core;

/**
 * Created by wrlxs on 26-5-2017.
 */
public class Coord {
    private int y;
    private int x;

    public Coord(int x, int y){
        this.y = y;
        this.x = x;
    }

    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }
}
