package Core;

import java.awt.image.RGBImageFilter;

/**
 * Created by Niels on 14-6-2017.
 */

import java.awt.Color;
import java.awt.image.RGBImageFilter;

    public class FilterText extends RGBImageFilter {

        private Color[] colors;
        public FilterText(Color[] colors){
            super();
            this.colors = colors;
        }
        private Color multColor(Color color) {
            for (int i = 0; i < colors.length; i++) {

                if (Utils.isBetween(color.getRed(), colors[i].getRed(), 3) && Utils.isBetween(color.getGreen(), colors[i].getGreen(), 3) && Utils.isBetween(color.getBlue(), colors[i].getBlue(), 3)) {
                    return new Color(0, 0, 0);

                }
            }
            return new Color(255, 255, 255);
        }


        public int filterRGB(int x, int y, int argb) {
            return multColor(new Color(argb)).getRGB();
        }
    }
