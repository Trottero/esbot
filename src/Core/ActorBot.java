package Core;

import java.awt.*;
import java.awt.event.InputEvent;
import java.awt.event.KeyEvent;
import java.util.ArrayList;

/**
 * Created by wrlxs on 30-5-2017.
 */
public class ActorBot extends Robot {
    public ActorBot() throws AWTException {
        super();
    }


    //ports ::home
    public void portHome(){
        keyPress(KeyEvent.VK_SHIFT);
        keyPress(KeyEvent.VK_SEMICOLON);
        keyRelease(KeyEvent.VK_SEMICOLON);
        keyPress(KeyEvent.VK_SEMICOLON);
        keyRelease(KeyEvent.VK_SEMICOLON);
        keyRelease(KeyEvent.VK_SHIFT);
        keyPress(KeyEvent.VK_H);
        keyRelease(KeyEvent.VK_H);
        keyPress(KeyEvent.VK_O);
        keyRelease(KeyEvent.VK_O);
        keyPress(KeyEvent.VK_M);
        keyRelease(KeyEvent.VK_M);
        keyPress(KeyEvent.VK_E);
        keyRelease(KeyEvent.VK_E);
        keyPress(KeyEvent.VK_ENTER);
        keyRelease(KeyEvent.VK_ENTER);
    }
    public void portBossing(){
        keyPress(KeyEvent.VK_SHIFT);
        keyPress(KeyEvent.VK_SEMICOLON);
        keyRelease(KeyEvent.VK_SEMICOLON);
        keyPress(KeyEvent.VK_SEMICOLON);
        keyRelease(KeyEvent.VK_SEMICOLON);
        keyRelease(KeyEvent.VK_SHIFT);
        keyPress(KeyEvent.VK_B);
        keyRelease(KeyEvent.VK_B);
        keyPress(KeyEvent.VK_O);
        keyRelease(KeyEvent.VK_O);
        keyPress(KeyEvent.VK_S);
        keyRelease(KeyEvent.VK_S);
        keyPress(KeyEvent.VK_S);
        keyRelease(KeyEvent.VK_S);
        keyPress(KeyEvent.VK_I);
        keyRelease(KeyEvent.VK_I);
        keyPress(KeyEvent.VK_N);
        keyRelease(KeyEvent.VK_N);
        keyPress(KeyEvent.VK_G);
        keyRelease(KeyEvent.VK_G);
        keyPress(KeyEvent.VK_ENTER);
        keyRelease(KeyEvent.VK_ENTER);
    }

    public void sellCommand(){
        keyPress(KeyEvent.VK_SHIFT);
        keyPress(KeyEvent.VK_SEMICOLON);
        keyRelease(KeyEvent.VK_SEMICOLON);
        keyPress(KeyEvent.VK_SEMICOLON);
        keyRelease(KeyEvent.VK_SEMICOLON);
        keyRelease(KeyEvent.VK_SHIFT);
        keyPress(KeyEvent.VK_S);
        keyRelease(KeyEvent.VK_S);
        keyPress(KeyEvent.VK_E);
        keyRelease(KeyEvent.VK_E);
        keyPress(KeyEvent.VK_L);
        keyRelease(KeyEvent.VK_L);
        keyPress(KeyEvent.VK_L);
        keyRelease(KeyEvent.VK_L);
        keyPress(KeyEvent.VK_ENTER);
        keyRelease(KeyEvent.VK_ENTER);
    }

    public void click(int x, int y){
        mouseMove(x, y);
        Utils.sleepMS(100);
        mousePress(InputEvent.BUTTON1_MASK);
        mouseRelease(InputEvent.BUTTON1_MASK);
    }
    public void click(Coord coord){
        mouseMove(coord.getX(), coord.getY());
        Utils.sleepMS(100);
        mousePress(InputEvent.BUTTON1_MASK);
        mouseRelease(InputEvent.BUTTON1_MASK);
    }
    public void clickCompass(){
        click(1770, 45);
    }

    public void moveMouseToMiddle(){
        mouseMove(1902/2 , 1080/2);
    }

    //searches for item in invent and returns Core.Coord
    public Coord findItem(Item item, int type){
        invList BeChecked = new invList(type);
        Coord[] toBeChecked = BeChecked.transFormList(item.transX, item.transY);


        for (int i = 0; i < toBeChecked.length; i++) {
            Color color = getPixelColor(toBeChecked[i].getX(), toBeChecked[i].getY());
            //System.out.println(toBeChecked[i].getX() + " " + toBeChecked[i].getY());
            if(Utils.isBetween(color.getRed(),item.r, 5) && Utils.isBetween(color.getGreen(),item.g, 5) && Utils.isBetween(color.getBlue(),item.b, 5)){
                System.out.println("Core.Item: " + item.itemName + " found at: " + toBeChecked[i].getX() + ", "+ toBeChecked[i].getY());
                return toBeChecked[i];
            }
        }
        System.out.println("Couldn't locate item: " + item.itemName);
        return null;
    }

    public ArrayList<Coord> findAllOfItem(Item item, int type){
        invList BeChecked = new invList(type);
        Coord[] toBeChecked = BeChecked.transFormList(item.transX, item.transY);

        ArrayList<Coord> listOfItems = new ArrayList<>();

        for (int i = 0; i < toBeChecked.length; i++) {
            Color color = getPixelColor(toBeChecked[i].getX(), toBeChecked[i].getY());
            if(Utils.isBetween(color.getRed(),item.r, 5) && Utils.isBetween(color.getGreen(),item.g, 5) && Utils.isBetween(color.getBlue(),item.b, 5)){
                //System.out.println("Core.Item: " + item.itemName + " found at: " + toBeChecked[i].getX() + ", "+ toBeChecked[i].getY());
                listOfItems.add(toBeChecked[i]);
            }
        }
        return listOfItems;
    }

    public void dropItem(Coord itemCoord, int option){
        mouseMove(itemCoord.getX(), itemCoord.getY());
        mousePress(InputEvent.BUTTON3_MASK);
        mouseRelease(InputEvent.BUTTON3_MASK);
        Utils.sleepMS(100);
        //956
        if(itemCoord.getY() < 956) {
            click(itemCoord.getX(), itemCoord.getY() + option);
        } else {
            click(itemCoord.getX(), 1002);
        }
        Utils.sleepMS(100);
        mouseMove(itemCoord.getX(), itemCoord.getY());
    }
    public void dropItemOption3(Coord itemCoord){
        mouseMove(itemCoord.getX(), itemCoord.getY());
        mousePress(InputEvent.BUTTON3_MASK);
        mouseRelease(InputEvent.BUTTON3_MASK);
        Utils.sleepMS(150);
        //956
        if(itemCoord.getY() < 944) {
            click(itemCoord.getX(), itemCoord.getY() + 60);
        } else {
            click(itemCoord.getX(), 1002);
        }
        Utils.sleepMS(100);
        mouseMove(itemCoord.getX(), itemCoord.getY());
    }

    public void dropAllOf(Item item){
        ArrayList<Coord> toBeDropped = findAllOfItem(item, 1);
        for (int i = 0; i < toBeDropped.size(); i++) {
            dropItem(toBeDropped.get(i), 40);
        }
    }

    public void dropAll(){
        Coord[] toBeDropped = new invList(1).getInvcoords();
        for (int i = 0; i < toBeDropped.length; i++) {
            //toBeDropped[i] = new Coord(toBeDropped[i].getX(), toBeDropped[i].getY() + 10);
            dropItemOption3(toBeDropped[i]);
        }
    }

    public void sellXOfItem(Item item){
        Coord itemCoord = findItem(item, 1);

        mouseMove(itemCoord.getX(), itemCoord.getY());
        Utils.sleepMS(500);
        mousePress(InputEvent.BUTTON3_MASK);
        mouseRelease(InputEvent.BUTTON3_MASK);
        Utils.sleepMS(500);

        if(itemCoord.getY() < 951) {
            click(itemCoord.getX(), itemCoord.getY() + 88);
        } else {
            click(itemCoord.getX(), 985);
        }

        Utils.sleepMS(200);

        //checks for thing to come up
        Color color = getPixelColor(400, 900);
        while(!Utils.isBetween(color.getRed(),214, 5) && !Utils.isBetween(color.getGreen(),195, 5) && !Utils.isBetween(color.getBlue(),165, 5)){
            System.out.println("Waiting for x amount to open");
            Utils.sleepMS(500);
            color = getPixelColor(400, 900);
        }
        Utils.sleepMS(200);

        keyPress(KeyEvent.VK_2);
        keyRelease(KeyEvent.VK_2);
        keyPress(KeyEvent.VK_8);
        keyRelease(KeyEvent.VK_8);
        keyPress(KeyEvent.VK_ENTER);
        keyRelease(KeyEvent.VK_ENTER);
        Utils.sleepMS(200);

        color = getPixelColor(400, 900);
        while(Utils.isBetween(color.getRed(),214, 5) && Utils.isBetween(color.getGreen(),195, 5) && Utils.isBetween(color.getBlue(),165, 5)){
            System.out.println("Waiting for x amount to close");

            Utils.sleepMS(500);
            color = getPixelColor(400, 900);
        }
        Utils.sleepMS(200);
        //mouseMove(itemCoord.getX(), itemCoord.getY());
    }


    public void openShop(){

        //dropAllOf(Item.BURNT_TUNA);
        //check if the level up thing is open.
        closeDialogIfOpen();
        sellCommand();
        Utils.sleepMS(500);

        Color color = getPixelColor(982, 319);
        while(!Utils.isBetween(color.getRed(),74, 5) && !Utils.isBetween(color.getGreen(),67, 5) && !Utils.isBetween(color.getBlue(),51, 5)) {
            System.out.println("Waiting for shop to open");
            Utils.sleepMS(500);
            color = getPixelColor(982, 319);
        }
    }

    public void buyItemFromShop(Item item) {
        Coord itemCoord = findItem(item, 2);

        if (itemCoord == null) {
            return;
        }
        mouseMove(itemCoord.getX(), itemCoord.getY());
        Utils.sleepMS(300);
        mousePress(InputEvent.BUTTON3_MASK);
        mouseRelease(InputEvent.BUTTON3_MASK);
        Utils.sleepMS(200);
        click(itemCoord.getX(), itemCoord.getY() + 88);
        Utils.sleepMS(200);

        //checks for thing to come up
        Color color = getPixelColor(400, 900);
        while (!Utils.isBetween(color.getRed(), 214, 5) && !Utils.isBetween(color.getGreen(), 195, 5) && !Utils.isBetween(color.getBlue(), 165, 5)) {
            System.out.println("Waiting for x amount to open");
            Utils.sleepMS(500);
            color = getPixelColor(400, 900);
        }
        Utils.sleepMS(300);

        keyPress(KeyEvent.VK_2);
        keyRelease(KeyEvent.VK_2);
        keyPress(KeyEvent.VK_8);
        keyRelease(KeyEvent.VK_8);
        keyPress(KeyEvent.VK_ENTER);
        keyRelease(KeyEvent.VK_ENTER);
        Utils.sleepMS(300);

        color = getPixelColor(400, 900);
        while (Utils.isBetween(color.getRed(), 214, 5) && Utils.isBetween(color.getGreen(), 195, 5) && Utils.isBetween(color.getBlue(), 165, 5)) {
            System.out.println("Waiting for x amount to close");
            Utils.sleepMS(500);
            color = getPixelColor(400, 900);
        }
        Utils.sleepMS(200);
    }

    public void closeShop(){
        Color color = getPixelColor(982, 319);
        if(Utils.isBetween(color.getRed(), 74, 5) && Utils.isBetween(color.getGreen(), 67, 5) && Utils.isBetween(color.getBlue(), 51, 5)) {
            click(1090, 319);

            while (Utils.isBetween(color.getRed(), 74, 5) && Utils.isBetween(color.getGreen(), 67, 5) && Utils.isBetween(color.getBlue(), 51, 5)) {
                System.out.println("waiting for shop to close");
                Utils.sleepMS(500);
                color = getPixelColor(982, 319);
            }
        }else{
            System.out.println("Shop wasn't open my friend");
        }
    }

    public void closeDialogIfOpen(){
        Color color = getPixelColor(400, 900);
        while(Utils.isBetween(color.getRed(),214, 20) && Utils.isBetween(color.getGreen(),195, 20) && Utils.isBetween(color.getBlue(),165, 20)){
            click(970, 545);
            Utils.sleepMS(500);
            color = getPixelColor(400, 900);
        }
    }
}
