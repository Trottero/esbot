package Core;

import org.w3c.dom.css.Rect;

import java.awt.*;
import java.awt.image.BufferedImage;
import java.awt.image.FilteredImageSource;
import java.awt.image.ImageFilter;
import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by Niels on 28-6-2017.
 */
public class ImageRecognizer {
    Robot bot;
    Rectangle rect;
    Color[] colors;
    HashMap<BufferedImage, String> imgToInt;
    ArrayList<BufferedImage> traindata;

    public ImageRecognizer(Rectangle rect, String font, Color[] colorsToFilter){
        this.rect = rect;
        this.colors = colorsToFilter;
        testSet testset = new testSet(font);
        imgToInt = testset.getImgToInt();
        traindata = testset.getTraindata();
        try {
            bot = new ActorBot();
        } catch (AWTException e) {
            e.printStackTrace();
        }
    }
    public String doOCR(){
        //capture image
        BufferedImage capture = bot.createScreenCapture(rect);

        //filters out the text by checking the colours =)
        ImageFilter colorfilter = new FilterText(colors);
        Image img = Toolkit.getDefaultToolkit().createImage(new FilteredImageSource(capture.getSource(), colorfilter));
        BufferedImage fullImage = new BufferedImage(img.getWidth(null), img.getHeight(null), BufferedImage.TYPE_INT_RGB);

        fullImage.getGraphics().drawImage(img,0,0, null);

        //splice letters into individual images/
        ArrayList<BufferedImage> results = new LetterSplicer().splice(fullImage);

        String endResult = "";
        for (int i = 0; i < results.size(); i++) {
            endResult = endResult + imgToInt.get(new imageComparator().getNumberFromImage(results.get(i), traindata));
        }
        if(endResult.isEmpty()){
            System.err.println("There were no letters found in the given area");
        }
        return endResult;
    }
}
