package Core;

import java.util.concurrent.TimeUnit;

/**
 * Created by wrlxs on 30-5-2017.
 */
public class Utils {
    public static boolean isBetween(int found, int expected, int maxDiff){
        return (found > expected - maxDiff && found < expected + maxDiff);
    }

    public static void sleepSecond(int x){
        try {
            TimeUnit.SECONDS.sleep(x);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public static void sleepMS(int x){
        try {
            TimeUnit.MILLISECONDS.sleep(x);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public static final boolean containsDigit(String s) {
        boolean containsDigit = false;

        if (s != null && !s.isEmpty()) {
            for (char c : s.toCharArray()) {
                if (containsDigit = Character.isDigit(c)) {
                    break;
                }
            }
        }

        return containsDigit;
    }
}
