package Core;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.awt.image.DataBuffer;
import java.io.File;
import java.io.IOException;
import java.nio.Buffer;
import java.util.ArrayList;

/**
 * Created by Niels on 15-6-2017.
 */
public class imageComparator {
    public imageComparator(){

    }

    public BufferedImage getNumberFromImage(BufferedImage imageA, ArrayList<BufferedImage> trainingSet){
        for (int i = 0; i < trainingSet.size(); i++) {
            if(compareImage(imageA, trainingSet.get(i))){
                return trainingSet.get(i);
            }
        }
                        try {
                    ImageIO.write(imageA, "PNG", new File("ERROR_FILE"+".PNG"));
                } catch (IOException e) {
                    e.printStackTrace();
                }
        throw new IllegalArgumentException("Not a single image corresponts! SMH.");
    }

    private boolean compareImage(BufferedImage imageA, BufferedImage imageB) {

        if (imageA.getWidth() == imageB.getWidth() && imageA.getHeight() == imageB.getHeight()) {
            for (int x = 0; x < imageA.getWidth(); x++) {
                for (int y = 0; y < imageA.getHeight(); y++) {
                    if (imageA.getRGB(x, y) != imageB.getRGB(x, y))
                        return false;
                }
            }
        } else {
            return false;
        }
        return true;
    }
}
