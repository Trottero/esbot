package Core;

/**
 * Created by wrlxs on 30-5-2017.
 */
public enum Item {
    //751, 773
    //1754, 770
    SUPER_RESTORE(162, 42, 85, 0, 0, "Super restore"),
    PRAYER_POTION(41, 206, 144, 0, 0, "Prayer potion"),
    MAGIC_POTION(162, 42, 85, 0, 0, "Magic potion"),
    SARADOMIN_BREW(41, 206, 144, 0, 0, "Saradomin brew"),
    BURNT_TUNA(92, 84, 83, -7, -2, "Burnt tuna"),
    COOKED_TUNA(164, 127, 120, -7, -2, "Cooked tuna"),
    RAW_TUNA(223, 219, 219, -7, -2, "Raw tuna"),
    RAW_SHARK(128, 116, 115, -9, 0, "Raw shark"),
    COOKED_SHARK(164, 124, 95, -9, 0, "Cooked shark"),
    BURNT_SHARK(66, 61, 61, -9, 0, "Burnt shark"),
    RAW_SWORDFISH(156, 123, 163, -10, -4, "Raw swordfish"),
    COOKED_SWORDFISH(137, 70, 149, -10, -4, "Cooked swordfish"),
    BURNT_SWORDFISH(67, 62, 62, -10, -4, "Burnt swordfish"),
    ORE_ADAMANTITE(53, 72, 54, 6, -10, "Adamantite ore"),
    ORE_MITHRIL(52, 52, 86, 6, -9, "Mithril ore"),
    ORE_RUNITE(55, 82, 93, 6, -9, "Runite ore"),
    BAR_ADAMANTITE(35, 49, 35, 3, -3, "Adamantite bar"),
    BAR_MITHRIL(35, 35, 58, 3, -3, "Mithril bar"),
    BAR_RUNITE(37, 53, 64, 3, -3, "Runite bar"),
    COOKED_ROCKTAIL(178, 139, 140, 2, -1, "Cooked rocktail"),
    RAW_ROCKTAIL(85, 114, 85, 2, -1, "raw rocktail"),
    RING_OF_WEALTH(119, 9, 168, 4, -22, "Ring of wealth"),
    SEERS_RING(250, 185, 65, -9, -16, "Seers ring (i)"),
    CUT_ONYX(72, 72, 79, -1, -10, "Cut Onyx");

    public final int r;
    public final int g;
    public final int b;
    public final int transX;
    public final int transY;
    public final String itemName;

    private Item(int r, int g, int b, int transX, int transY, String itemName) {
        this.r = r;
        this.g = g;
        this.b = b;
        this.transX = transX;
        this.transY = transY;
        this.itemName = itemName;
    }
}
