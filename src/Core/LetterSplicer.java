package Core;

import java.awt.*;
import java.awt.image.BufferedImage;
import java.util.ArrayList;

/**
 * Created by Niels on 15-6-2017.
 */
public class LetterSplicer {
    int heigth;
    int width;
    BufferedImage orig;
    ArrayList<BufferedImage> result = new ArrayList<BufferedImage>();
    public LetterSplicer(){
    }
    public ArrayList<BufferedImage> splice (BufferedImage orig){
        width = orig.getWidth();
        heigth = orig.getHeight();
        this.orig = orig;
        int lastColour = 0;

        int imgstartX = 0;
        int imgendX = 0;
        for (int i = 0; i < width; i++) {
            if(checkIfLineIsWhiteX(i)){
            } else {
                for (int j = i; j < width; j++) {
                    if(!checkIfLineIsWhiteX(j)){
                    } else {
                        break;
                    }
                    imgendX = j- i;
                }

                BufferedImage subimg = orig.getSubimage(i, 0, imgendX + 1, heigth); //fill in the corners of the desired crop location here
                BufferedImage copyOfImage = new BufferedImage(subimg.getWidth(), subimg.getHeight(), BufferedImage.TYPE_INT_RGB);
                Graphics g = copyOfImage.createGraphics();
                g.drawImage(subimg, 0, 0, null);

                copyOfImage = trimTopAndBottom(copyOfImage);
                result.add(copyOfImage);
                i = i + imgendX;
            }
        }

        return result;
    }

    public boolean checkIfLineIsWhiteX(int x){
        for (int i = 0; i < heigth; i++) {
            int clr=  orig.getRGB(x,i);
            int  red   = (clr & 0x00ff0000) >> 16;
            int  green = (clr & 0x0000ff00) >> 8;
            int  blue  =  clr & 0x000000ff;
            if(Utils.isBetween(red, 0, 1) && Utils.isBetween(green, 0, 1) && Utils.isBetween(blue, 0, 1)){
                return false;
            }
        }
        return true;
    }
    public boolean checkIfLineIsWhiteY(int y, BufferedImage img){
        for (int i = 0; i < img.getWidth(); i++) {
            int clr=  img.getRGB(i,y);
            int  red   = (clr & 0x00ff0000) >> 16;
            int  green = (clr & 0x0000ff00) >> 8;
            int  blue  =  clr & 0x000000ff;
            if(Utils.isBetween(red, 0, 1) && Utils.isBetween(green, 0, 1) && Utils.isBetween(blue, 0, 1)){
                return false;
            }
        }
        return true;
    }

    public BufferedImage trimTopAndBottom(BufferedImage orig2){
        BufferedImage orig3 = orig2;
        if (checkIfLineIsWhiteY(0, orig3)) {
            BufferedImage subimg = orig3.getSubimage(0, 1, orig3.getWidth(), orig3.getHeight()-1); //fill in the corners of the desired crop location here
            BufferedImage copyOfImage = new BufferedImage(subimg.getWidth(), subimg.getHeight(), BufferedImage.TYPE_INT_RGB);
            Graphics g = copyOfImage.createGraphics();
            g.drawImage(subimg, 0, 0, null);
            return trimTopAndBottom(copyOfImage);
        }
        if (checkIfLineIsWhiteY(orig3.getHeight() - 1, orig3)) {
            BufferedImage subimg = orig3.getSubimage(0, 0, orig3.getWidth(), orig3.getHeight()-1); //fill in the corners of the desired crop location here
            BufferedImage copyOfImage = new BufferedImage(subimg.getWidth(), subimg.getHeight(), BufferedImage.TYPE_INT_RGB);
            Graphics g = copyOfImage.createGraphics();
            g.drawImage(subimg, 0, 0, null);
            return trimTopAndBottom(copyOfImage);
        }
        return orig3;
    }
}
