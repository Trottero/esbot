package Core;

/**
 * Created by wrlxs on 30-5-2017.
 */
public class invList {

    //default location of pixels to be checked -- left bottom pixel of the colour of a pot is default.
    private Coord[] invcoords;
    public invList(int x){
        if(x == 1){
        invcoords = new Coord[] {
                new Coord(1751, 773), new Coord(1793, 773), new Coord(1835, 773), new Coord(1877, 773),
                new Coord(1751, 809), new Coord(1793, 809), new Coord(1835, 809), new Coord(1877, 809),
                new Coord(1751, 845), new Coord(1793, 845), new Coord(1835, 845), new Coord(1877, 845),
                new Coord(1751, 881), new Coord(1793, 881), new Coord(1835, 881), new Coord(1877, 881),
                new Coord(1751, 917), new Coord(1793, 917), new Coord(1835, 917), new Coord(1877, 917),
                new Coord(1751, 953), new Coord(1793, 953), new Coord(1835, 953), new Coord(1877, 953),
                new Coord(1751, 989), new Coord(1793, 989), new Coord(1835, 989), new Coord(1877, 989)
            };
        }
        if (x == 2){
            invcoords = new Coord[] {
                    new Coord(648, 380), new Coord(700, 380), new Coord(752, 380), new Coord(804, 380),
                    new Coord(856, 380), new Coord(908, 380), new Coord(960, 380), new Coord(1012, 380), new Coord(1064, 380),

                    new Coord(648, 445), new Coord(700, 445), new Coord(752, 445), new Coord(804, 445),
                    new Coord(856, 445), new Coord(908, 445), new Coord(960, 445), new Coord(1012, 445), new Coord(1064, 445),

//                    new Coord(1751, 917), new Coord(1793, 917), new Coord(1835, 917), new Coord(1877, 917),
//                    new Coord(1751, 953), new Coord(1793, 953), new Coord(1835, 953), new Coord(1877, 953),
//                    new Coord(1751, 989), new Coord(1793, 989), new Coord(1835, 989), new Coord(1877, 989)
            };
        }
    }

    //allows for transformation of the list, maybe some sprites need another location to be tested hehexd
    public Coord[] transFormList(int transX, int transY){
        for (int i = 0; i < invcoords.length; i++) {
            invcoords[i] = new Coord(invcoords[i].getX() + transX, invcoords[i].getY() + transY);
        }
        return invcoords;
    }

    public Coord[] getInvcoords() {
        return invcoords;
    }
}
