package other;

/**
 * Created by Niels on 28-6-2017.
 */
    import Core.ActorBot;
    import org.jnativehook.GlobalScreen;
            import org.jnativehook.NativeHookException;
            import org.jnativehook.keyboard.NativeKeyEvent;
            import org.jnativehook.keyboard.NativeKeyListener;

    import java.awt.*;

class GlobalKeyListener implements NativeKeyListener {
    private static ActorBot bot;
    public void nativeKeyPressed(NativeKeyEvent e) {

        //System.out.println("Key Pressed: " + NativeKeyEvent.getKeyText(e.getKeyCode()));

        if (e.getKeyCode() == NativeKeyEvent.VC_ESCAPE) {
            try {
                GlobalScreen.unregisterNativeHook();
            } catch (NativeHookException e1) {
                e1.printStackTrace();
            }
        }

        if (e.getKeyCode() == NativeKeyEvent.VC_BACK_SLASH) {
                bot.dropAll();
        }
    }

    public void nativeKeyReleased(NativeKeyEvent e) {
    }

    public void nativeKeyTyped(NativeKeyEvent e) {
    }

    public static void main(String[] args) {
        try {
            bot = new ActorBot();
        } catch (AWTException e) {
            e.printStackTrace();
        }
        try {
            GlobalScreen.registerNativeHook();
        }
        catch (NativeHookException ex) {
            System.err.println("There was a problem registering the native hook.");
            System.err.println(ex.getMessage());

            System.exit(1);
        }

        //Construct the example object and initialze native hook.
        GlobalScreen.addNativeKeyListener(new GlobalKeyListener());
    }
}
